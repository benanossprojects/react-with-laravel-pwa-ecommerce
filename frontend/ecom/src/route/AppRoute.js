import React, { Component, Fragment } from 'react'
import { Route, Routes } from "react-router";
import HomePage from '../pages/HomePage';
import UserLoginPage from '../pages/UserLoginPage';
import ContactPage from "../pages/ContactPage";
import PrivacyPage from "../pages/PrivacyPage";
import RefundPage from "../pages/RefundPage";
import AboutPage from "../pages/AboutPage";
import PurchasePage from "../pages/PurchasePage";


class AppRoute extends Component {
     render() {
          return (
               <Fragment>
                   <Routes>
                       <Route path="/" element={<HomePage />} />
                       <Route exact path="/login" element={<UserLoginPage />} />
                       <Route exact path="/about" element={<AboutPage />} />
                       <Route exact path="/contact" element={<ContactPage />} />
                       <Route exact path="/privacy" element={<PrivacyPage />} />
                       <Route exact path="/refund" element={<RefundPage />} />
                       <Route exact path="/purchase" element={<PurchasePage />} />

                   </Routes>


               </Fragment>
          )
     }
}

export default AppRoute
